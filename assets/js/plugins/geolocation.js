$("#define_city").click(getLocation);

	function getLocation() {
   		console.log("start geolocation");
		if (navigator.geolocation) {
			console.log("start_geolocation");
	        navigator.geolocation.getCurrentPosition(showPosition, showError);
	    } else {
	        console.log = "Geolocation is not supported by this browser.";
	    }
	}

	    function showPosition(position) {
		    var latitude = position.coords.latitude;
		    var longitude = position.coords.longitude;
		    var latlon = latitude + "," + longitude;
		    lotlan = longitude + "," + latitude;
		    var geocode_url = "http://geocode-maps.yandex.ru/1.x/?format=json&kind=locality&results=1&geocode="+lotlan;
		    $.get( geocode_url, function( data ) {
		        city = data.response.GeoObjectCollection.featureMember[0].GeoObject.name;
		        $( "#input_city" ).val(city);
		    });
		    //console.log(lotlan);
		}

		function showError(error) {
		    switch(error.code) {
		        case error.PERMISSION_DENIED:
		            console.log="User denied the request for Geolocation.";
		            break;
		        case error.POSITION_UNAVAILABLE:
		            console.log="Location information is unavailable.";
		            break;
		        case error.TIMEOUT:
		            console.log="The request to get user location timed out.";
		            break;
		        case error.UNKNOWN_ERROR:
		            console.log="An unknown error occurred.";
		            break;
		    }
		}