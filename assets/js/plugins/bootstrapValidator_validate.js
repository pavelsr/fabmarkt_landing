$( document ).ready(function() {
$('#call-form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            contact_name: {
	                message: 'Поле заполнено неверно',
	                validators: {
	                    notEmpty: {
	                        message: 'Это обязательное поле и оно не может быть пустым'
	                    },
	                }
	            },
	            comments: {
	                message: 'Поле заполнено неверно',
	                validators: {
	                    notEmpty: {
	                        message: 'Это обязательное поле и оно не может быть пустым'
	                    },
	                }
	            },
	            phone : {
	            	message: 'Телефон указан неверно',
	                validators: {
	                    notEmpty: {
	                        message: 'Это обязательное поле и оно не может быть пустым'
	                    }
	                }
	            },
	            email: {
	                validators: {
	                    notEmpty: {
	                        message: 'Это обязательное поле и оно не может быть пустым'
	                    },
	                    emailAddress: {
	                        message: 'Email указан неверно'
	                    }
	                }
	            }
	        }
    	});
});