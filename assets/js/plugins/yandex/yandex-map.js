window.onload = loadScript;

function loadScript() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'http://api-maps.yandex.ru/2.1/?lang=ru_RU&onload=initialize';
	document.body.appendChild(script);
}

function initialize(ymaps) {
    var myMap;
        myMap = new ymaps.Map("map-canvas", {
            center: [54.80, 82.75],
            zoom: 3,
            controls: ["fullscreenControl", "geolocationControl"],
            maxZoom: 5
        });

        myMap.behaviors.disable('scrollZoom');


        $.get( "http://fabnews.fablab61.ru/all_locations", function( data ) {
            console.log(data.length);
            for (var i = 0; i < data.length; i++) {
                var latlon = new Array(data[i].loc.coordinates[1], data[i].loc.coordinates[0]); 
                myMap.geoObjects.add(new ymaps.Placemark(latlon));
            }
        });
}

