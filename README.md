# README #

This is HTML+CSS+JS of fabmarkt.ru landings

## Used third-party code ##

* Icons from [Font-Awesome](http://fortawesome.github.io/Font-Awesome/)
* [Bootstrap](http://getbootstrap.com/)
* [JQuery](https://jquery.com/)
* [JQuery Vide](http://vodkabears.github.io/vide/) for background video
* [JPreloader](https://github.com/kennyooi/jpreloader) as preloader

Call action form do POST request to http://api.fabmarkt.ru (POST and validation is done by
form_serialize.js)

